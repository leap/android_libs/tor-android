package org.torproject.jni;

public interface ClientTransportPluginInterface {
    void start();
    void stop();
    String getTorrc();
}
