apply plugin: 'com.android.library'

/* gets the version name from the latest Git tag, stripping the leading 'tor-' off */
def getVersionName = { ->
    def stdout = new ByteArrayOutputStream()
    exec {
        commandLine 'git', 'describe', '--tags', '--always'
        standardOutput = stdout
    }
    return stdout.toString().trim()
}

android {
    compileSdkVersion 31
    buildToolsVersion '31.0.0'

    defaultConfig {
        minSdkVersion 16
        targetSdkVersion 31
        versionCode 41500
        versionName getVersionName()

        testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'
        /*
          The Android Testing Support Library collects analytics to continuously improve the testing
          experience. More specifically, it uploads a hash of the package name of the application
          under test for each invocation. If you do not wish to upload this data, you can opt-out by
          passing the following argument to the test runner: disableAnalytics "true".
         */
        testInstrumentationRunnerArguments disableAnalytics: 'true'
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }

    packagingOptions {
        exclude 'META-INF/androidx.localbroadcastmanager_localbroadcastmanager.version'
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.so'])
    api 'androidx.localbroadcastmanager:localbroadcastmanager:1.0.0'
    api 'info.guardianproject:jtorctl:0.4.5.7'

    androidTestImplementation 'androidx.test:core:1.4.0'
    androidTestImplementation 'androidx.test:runner:1.4.0'
    androidTestImplementation 'androidx.test:rules:1.4.0'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'info.guardianproject.netcipher:netcipher:2.1.0'
    androidTestImplementation 'commons-io:commons-io:2.6'
    androidTestImplementation 'commons-net:commons-net:3.6'
}


android.libraryVariants.all { variant ->
    def name = variant.buildType.name
    if (name.equals(com.android.builder.core.BuilderConstants.DEBUG)) {
        return; // Skip debug builds.
    }
    def task = project.tasks.create "jar${name.capitalize()}", Jar
    task.dependsOn variant.javaCompile, sourcesJar, javadocJar
    task.from variant.javaCompile.destinationDir
    task.exclude('info/guardianproject/**/BuildConfig.**')
    artifacts.add('archives', task);
    task.baseName = 'tor-android'
}

tasks.withType(AbstractArchiveTask) {
    dirMode = 0755
    fileMode = 0644
    preserveFileTimestamps = false
    reproducibleFileOrder = true
}

task sourcesJar(type: Jar) {
    from android.sourceSets.main.java.srcDirs
    classifier = 'sources'
    baseName = 'tor-android-' + getVersionName()
}

configurations {
    libconfiguration
    // declare a configuration that is going to resolve the compile classpath of the application
    compileClasspath.extendsFrom(libconfiguration)

    // declare a configuration that is going to resolve the runtime classpath of the application
    runtimeClasspath.extendsFrom(libconfiguration)
}

task javadoc(type: Javadoc) {
    source = android.sourceSets.main.java.srcDirs
    classpath += project.files(android.getBootClasspath().join(File.pathSeparator))
    classpath += configurations.libconfiguration
    options.noTimestamp = true
    options.addStringOption('charset', 'UTF-8') // to match Maven's case
    android.libraryVariants.all { variant ->
        if (variant.name == 'release') {
            owner.classpath += variant.javaCompile.classpath
        }
    }
    exclude '**/R.html', '**/R.*.html', '**/index.html'
    failOnError false
}

task javadocJar(type: Jar, dependsOn: javadoc) {
    classifier = 'javadoc'
    from javadoc.destinationDir
    baseName = 'tor-android-' + getVersionName()
}


apply plugin: 'maven-publish'

